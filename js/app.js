/**
 * Created by kingandrew1024 on 5/October/18.
 */

"use strict";

angular.module("chatAdmin", [])
  .run(function ($rootScope) {
    $rootScope.totalUsers = 0;
    $rootScope.userList = {};

    var socket = io.connect(
      'http://http://ec2-52-13-211-102.us-west-2.compute.amazonaws.com:3000/', //the DEVELOPMENT remote server
      //'http://ec2-54-69-184-92.us-west-2.compute.amazonaws.com:3000/', //the PRODUCTION remote server
      //'http://localhost:3000'//localhost server
    );

    socket.on('connect', function () {
      console.log("Chat admin is now connected!");
      $rootScope.userList = {};
      socket.emit("getUserlist", null, function (data) {
        //console.log("on getUserlist...", data);
        $rootScope.totalUsers = data.totalUsers;
        data.users.forEach(function (user) {
          $rootScope.userList[user.appId] = user;
        });

        $rootScope.$apply();
      });
    });



    socket.on('admin:user.connected',
      /**
       * @param data {{
       *   user: {appId:number, socketId:string, name:string, avatar:string, roomId: number},
       *   totalUsers:number
       * }}
       */
      function (data) {
        $rootScope.userList[data.user.appId] = data.user;
        $rootScope.totalUsers = data.totalUsers;
        $rootScope.$apply()
      });

    socket.on('admin:user.disconnected',
      /**
       * @param data {{
       *   user: {appId:number, socketId:string, name:string, avatar:string, roomId: number},
       *   totalUsers:number
       * }}
       */
      function (data) {
        delete $rootScope.userList[data.user.appId];
        $rootScope.totalUsers = data.totalUsers;
        $rootScope.$apply()
      });

    socket.on('admin:user.room.changed',
      /**
       * @param user {{appId:number, socketId: string, name: string, avatar: string, roomId: number}}
       */
      function (user) {
        $rootScope.userList[user.appId].roomId = user.roomId;
        $rootScope.$apply();
      });
  });